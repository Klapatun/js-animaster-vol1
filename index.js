"use strict";

(function main() {
  /*steps*/
  document.getElementById('stepsPlay').addEventListener('click',function(){
    const block = document.getElementById('stepsBlock');
    animaster()
      .addMove(200, { x: 40, y: 40 })
      .addScale(800, 1.3)
      .addMove(200, { x: 80, y: 0 })
      .addScale(800, 1)
      .addMove(200, { x: 40, y: -40 })
      .addScale(800, 0.7)
      .addMove(200, { x: 0, y: 0 })
      .addScale(800, 1)
      .addFadeOut(1000)
      .addShowAndHide(1000)
      .addFadeIn(500)
      .addMoveAndHide(1000)
      .play(block);
  })
  /* FadeIn */
  document.getElementById('fadeInPlay').addEventListener('click', function () {
    const block = document.getElementById('fadeInBlock');
    // animaster().fadeIn(block, 5000);
    animaster().addFadeIn(5000).play(block);
  });

  document.getElementById('fadeInStop').addEventListener('click', function () {
    const block = document.getElementById('fadeInBlock');
    animaster().resetfadeIn(block);
  });

  /* FadeOut */
  document.getElementById('fadeOutPlay').addEventListener('click', function () {
    const block = document.getElementById('fadeOutBlock');
    // animaster().fadeOut(block, 5000);
    animaster().addFadeOut(5000).play(block);
  });

  document.getElementById('fadeOutStop').addEventListener('click', function () {
    const block = document.getElementById('fadeOutBlock');
    animaster().resetfadeOut(block);
  });

  /* move */
  document.getElementById('movePlay').addEventListener('click', function () {
    const block = document.getElementById('moveBlock');
    // animaster().move(block, 1000, { x: 100, y: 10 });
    animaster().addMove(1000, {x: 100, y: 10}).play(block);
  });
  document.getElementById('moveStop').addEventListener('click', function () {
    const block = document.getElementById('moveBlock');
    animaster().resetMove(block);
  });

  /* moveAndHide */
  document
    .getElementById('moveAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      // animaster().moveAndHide(block, 1000);
      animaster().addMoveAndHide(1000).play(block);
    });
  document
    .getElementById('moveAndHideStop')
    .addEventListener('click', function () {
      const block = document.getElementById('moveAndHideBlock');
      animaster().resetMoveAndHide(block);
    });
  /* scale */
  document.getElementById('scalePlay').addEventListener('click', function () {
    const block = document.getElementById('scaleBlock');
    // animaster().scale(block, 1000, 1.25);
    animaster().addScale(1000, 1.25).play(block);
  });

  document.getElementById('scaleStop').addEventListener('click', function () {
    const block = document.getElementById('scaleBlock');
    animaster().resetScale(block);
  });
  /*showAndHide*/
  document
    .getElementById('showAndHidePlay')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      // animaster().showAndHide(block, 1000);
      animaster().addShowAndHide(1000).play(block);
    });

  document
    .getElementById('showAndHideStop')
    .addEventListener('click', function () {
      const block = document.getElementById('showAndHideBlock');
      animaster().resetShowAndHide(block);
    });
  {
    //отдельная область видимости для таймеров
    /*heartBeating*/
    let heartBeatingTimerId = { stop: function () {} };
    document
      .getElementById('heartBeatingPlay')
      .addEventListener('click', function () {
        const block = document.getElementById('heartBeatingBlock');
        heartBeatingTimerId = animaster().heartBeating(block, 500, 1.4);
      });
    document
      .getElementById('heartBeatingStop')
      .addEventListener('click', function () {
        heartBeatingTimerId.stop();
      });
    /*shaking*/
    let shakingTimerId = { stop: function () {} };
    document
      .getElementById('shakingPlay')
      .addEventListener('click', function () {
        const block = document.getElementById('shakingBlock');
        shakingTimerId = animaster().shaking(block, 500, { x: 20, y: 0 });
      });
    document
      .getElementById('shakingStop')
      .addEventListener('click', function () {
        shakingTimerId.stop();
      });
  }
})();

function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(' ');
}

function animaster() {
  let obj = {
    _steps: [],
    
    addMove: function (duration,translation) {
      this._steps.push({
        name:'move',
        duration:duration,
        translation:translation,
      })
      return this;
    },
    addFadeIn:function (duration){
      this._steps.push({
        name:'fadeIn',
        duration:duration,
      })
      return this;
    },
    addFadeOut: function (duration) {
      this._steps.push({
        name: 'fadeOut',
        duration: duration
      });
      return this;
    },
    addScale:function (duration, ratio) {
      this._steps.push({
        name: 'scale',
        duration: duration,
        ratio: ratio
      });
      return this;
    },
    addDelay: function(duration) {
      this._steps.push({
        name: 'delay',
        duration: duration
      });
      return this;
    },

    addMoveAndHide: function (duration) {
      // this._steps.push({
      //   name: 'moveAndHide',
      //   duration: duration
      // });
      this.addMove(duration * (2 / 5), { x: 100, y: 20 });
      this.addFadeOut(duration * (3 / 5));
      return this;
    },
    addShowAndHide: function (duration) {
      this.addFadeIn(duration*(1/3));
      this.addDelay(duration*(1/3));
      this.addFadeOut(duration*(1/3));
      return this;
    },
    addHeartBeating: function (duration, ratio) {
      this._steps.push({
        name: 'heartBeating',
        duration: duration,
        ratio: ratio
      });
      return this;
    },
    
    play:function(element){
      let acc = 0;
      let currentPosition = 0;
      this._steps.forEach(action=>
      {switch(action.name)
      {
        case 'move':
         setTimeout(this.move.bind(this),acc,element,action.duration,action.translation);
         currentPosition = action.translation;
         break;
        case 'fadeIn':
          setTimeout(this.fadeIn.bind(this), acc, element, action.duration);
          // animaster().fadeIn(element,action.duration);
          break;
        case 'fadeOut':
          setTimeout(this.fadeOut.bind(this), acc, element, action.duration);
          break;
        case 'scale':
          setTimeout(this.scale.bind(this),acc,element,action.duration,action.ratio,currentPosition);//(element,action.duration,action.ratio);
          break;
        case 'moveAndHide':
          setTimeout(this.moveAndHide.bind(this), acc, element, action.duration);
          break;
        case 'delay':
          setTimeout(this.delay.bind(this), acc, action.duration);
          break;
        // default:
        }
        
        acc+=20+action.duration;
      });
      
    },
    scale: function (element, duration, ratio,translation = null) {
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(translation, ratio);
    },
    resetScale: function (element) {
      element.style.transition = 'all';
      element.style.transform = 'none';
    },
    fadeIn: function (element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove('hide');
      element.classList.add('show');
    },
    resetfadeIn: (element) => {
      element.style.transition = 'all';
      element.classList.remove('show');
      element.classList.add('hide');
    },
    move: function (element, duration, translation) {
      element.style.transitionDuration = `${duration}ms`;
      element.style.transform = getTransform(translation, null);
    },
    resetMove: function (element) {
      element.style.transition = 'all';
      element.style.transform = 'none';
    },
    moveAndHide: function (element, duration) {
      animaster().move(element, duration * (2 / 5), { x: 100, y: 20 });
      animaster().fadeOut(element, duration * (3 / 5));
    },
    resetMoveAndHide: function (element) {
      element.style.transition = 'all';
      element.style.transform = 'none';
      element.classList.remove('hide');
      element.classList.add('show');
    },
    fadeOut: function (element, duration) {
      element.style.transitionDuration = `${duration}ms`;
      element.classList.remove('show');
      element.classList.add('hide');
    },
    resetfadeOut: function (element) {
      element.style.transition = 'all';
      element.classList.remove('hide');
      element.classList.add('show');
    },

    delay: async function (duration) {
      function sleep(ms) {
        return new Promise((resolve)=>setTimeout(resolve,ms));
      };

      await sleep(duration);
    },

    // showAndHide: function (duration) {
    //   // console.log('showAndHide');
    //   // this.addFadeOut(duration);
    //   // console.log(this);

    //   // this._steps.push({
    //   //   name: 'fadeOut',
    //   //   duration: (duration*(1/3))
    //   // });
      
    //   // animaster().addFadeIn(element, duration*(1/3));
    //   // animaster().addDelay(duration*(1/3));
    //   // animaster().addFadeOut(duration*(1/3));
    // },
    resetShowAndHide: function (element) {
      element.style.transition = 'all';
      element.classList.remove('show');
      element.classList.add('hide');
    },
    heartBeating: function (element, duration, ratio) {
      let heartBeatingTimerId = setTimeout(function tick() {
        animaster().scale(element, duration, ratio);
        setTimeout(animaster().scale, duration, element, duration, 1);
        heartBeatingTimerId = setTimeout(tick, duration * 3); //на значении в duration*2 анимация со временем ломается
      }, 0);
      return { stop: () => clearTimeout(heartBeatingTimerId) };
    },

    shaking: function (element, duration, translation) {
      const offset = { x: -translation.x, y: 0 };
      let shakingTimerId = setTimeout(function tick() {
        animaster().move(element, duration, translation);
        setTimeout(animaster().move, duration, element, duration, offset);
        shakingTimerId = setTimeout(tick, duration * 3); //на значении в duration*2 анимация со временем ломается
      }, 0);
      return { stop: () => clearTimeout(shakingTimerId) };
    },
  };
  return obj;
}
